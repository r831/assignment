import matplotlib
import numpy
import openpyxl
import pandas
import xlwings
import xlrd
import xlwt
from openpyxl import Workbook


# import xlsxWriter

path = "C:\Rupesh_IMP\python\EMPLOYEE_TEMP.xlsx"
test = pandas.read_excel(path)
# print(test)
test.head()

print("Excel Read Successfully.")

#Insert rows to excel

path = "C:\Rupesh_IMP\python\EMPLOYEE_TEMP.xlsx"
wb_obj = openpyxl.load_workbook(path.strip())
sheet_obj = wb_obj.active
print("Maximum rows before inserting:", sheet_obj.max_row)
# insert 2 rows starting on the first row
sheet_obj.insert_rows(idx=3)

# insert multiple rows at once
# insert 3 rows starting on the six row
sheet_obj.insert_rows(6, 3)

print("Maximum rows after inserting:", sheet_obj.max_row)

# save the file to the path
path = "C:\Rupesh_IMP\python\employee.xlsx"
#sheet_obj.save(path)
wb_obj.save(path)

print("Added Rows Successfully.")

#update existing value


sheet = xlwings.Book("C:\Rupesh_IMP\python\EMPLOYEE_TEMP.xlsx").sheets("Sheet1")
sheet.range("A2").value = "RUPESH BOMBALE"

print("Record updated Successfully.")

#Deleting existing row

# delete 2 rows starting on the third row
sheet_obj.delete_rows(idx=3, amount=2)

print("Record deleted Successfully.")